import React, { Component } from 'react'
import Input from './Input'
import './App.css'
import * as moment from 'moment'
import { connect } from 'react-redux'
import { setField, submit } from './reducer'

const targetDate = moment('2018-05-06 17:00:00')

class App extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.interval = setInterval(this.updateCountdown, 1000)
    this.updateCountdown()
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  updateCountdown = () => {
    const duration = moment.duration(targetDate.diff(moment()))
    const hour = ~~duration.asHours()
    const minute = duration.minutes()
    const second = duration.seconds()
    // this.props.setField(
    //   'countdown',
    //   `${hour} hours ${minute} minutes ${second} seconds`
    // )
  }

  render() {
    const {
      email,
      countdown,
      username,
      agreeTerms,
      addFood,
      setField
    } = this.props
    return (
      <div className="section">
        <div className="container">
          <form
            onSubmit={e => {
              e.preventDefault()
              this.props.submit(this.props)
            }}
          >
            <div className="title">Evenn Registeration Form</div>
            <p>Ticket sale ends in </p>
            <p>{countdown}</p>
            <Input
              type="email"
              title="Email"
              placeholder="Email"
              icon="fa-envelope"
              error="The email is invalid"
              value={email}
              onChange={email => setField('email', email)}
            />
            <Input
              type="email"
              title="Username"
              placeholder="Username"
              icon="fa-envelope"
              value={username}
              onChange={username => setField('username', username)}
              required
            />
            <div className="field">
              <label className="label">Ticket Type</label>
              <div className="control">
                <div className="select">
                  <select
                    onChange={e => setField('ticketType', e.target.value)}
                  >
                    <option>Select dropdown</option>
                    <option value="regular">Regular 100 THB</option>
                    <option value="premium">Premium 300 THB</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="field">
              <div className="control">
                <label className="checkbox">
                  <input
                    type="checkbox"
                    checked={agreeTerms}
                    onChange={e => setField('agreeTerms', e.target.checked)}
                  />
                  I agree to the <a href="#">terms and conditions</a>
                </label>
              </div>
            </div>
            <div className="field">
              <label className="label">Add food?</label>
              <div className="control">
                <label className="radio">
                  <input
                    type="radio"
                    name="question"
                    checked={addFood}
                    onChange={e => setField('addFood', true)}
                  />
                  Yes (+ 50 THB)
                </label>
                <label className="radio">
                  <input
                    type="radio"
                    name="question"
                    checked={!addFood}
                    onChange={e => setField('addFood', false)}
                  />
                  No
                </label>
              </div>
            </div>
            <div className="field is-grouped">
              <div className="control">
                <button
                  disabled={this.props.loading}
                  type="submit"
                  className="button is-link"
                >
                  Submit
                </button>
              </div>
              <div className="control">
                <button className="button is-text">Cancel</button>
              </div>
            </div>
            <p>{this.props.message}</p>
          </form>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({
  email,
  ticketType,
  agreeTerms,
  addFood,
  countdown,
  username,
  loading,
  message
}) => ({
  email,
  ticketType,
  agreeTerms,
  addFood,
  countdown,
  username,
  loading,
  message
})

const mapDispatchToProps = {
  setField,
  submit
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
